package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"time"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.RemoteAddr + " - " + r.Method + " " + r.RequestURI + " - " + r.UserAgent())
	requestDump, err := httputil.DumpRequest(r, true)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Header().Add("Content-Type", "text/plain; charset=utf-8")
	fmt.Fprintln(w, string(requestDump))
}

func main() {
	http.HandleFunc("/", helloHandler)

	fmt.Println("Started, serving at 8080")
	srv := &http.Server{
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		Addr:         ":8080",
	}

	if err := srv.ListenAndServe(); err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
